use HTTP::Cookies;
use LWP::UserAgent;
use HTTP::Request::Common qw(POST);


my $app="http://3.theanticorruptionpledge.appspot.com/";

my $ua = LWP::UserAgent->new(  );


my %STATES2 = (
    'AL'=>'Alabama', 
    'AK'=>'Alaska', 
    'AS'=>'American Samoa', 
);

#
my %STATES = (
    'AL'=>'Alabama', 
    'AK'=>'Alaska', 
    'AS'=>'American Samoa', 
    'AZ'=>'Arizona', 'AR'=>'Arkansas', 'CA'=>'California', 'CO'=>'Colorado', 'CT'=>'Connecticut', 'DC'=>'District of Columbia', 'DE'=>'Delaware', 'FL'=>'Florida', 'GA'=>'Georgia', 'GU'=>'Guam', 'HI'=>'Hawaii', 'ID'=>'Idaho', 'IL'=>'Illinois', 
    'IN'=>'Indiana', 
    'IA'=>'Iowa', 'KS'=>'Kansas', 'KY'=>'Kentucky', 'LA'=>'Louisiana', 'ME'=>'Maine', 
    'MD'=>'Maryland', 'MA'=>'Massachusetts', 'MI'=>'Michigan', 'MN'=>'Minnesota', 'MS'=>'Mississippi', 
    'MO'=>'Missouri', 'MP'=>'Northern Mariana Islands', 'MT'=>'Montana', 'NE'=>'Nebraska', 'NV'=>'Nevada', 
    'NH'=>'New Hampshire', 'NJ'=>'New Jersey', 'NM'=>'New Mexico', 'NY'=>'New York', 'NC'=>'North Carolina', 
    'ND'=>'North Dakota', 'OH'=>'Ohio', 'OK'=>'Oklahoma', 'OR'=>'Oregon', 'PA'=>'Pennsylvania', 'PR'=>'Puerto Rico', 'RI'=>'Rhode Island', 'SC'=>'South Carolina', 'SD'=>'South Dakota', 'TN'=>'Tennessee', 'TX'=>'Texas', 'UT'=>'Utah', 'VT'=>'Vermont', 'VA'=>'Virginia', 'VI'=>'Virgin Islands',
    'WA'=>'Washington', 'WV'=>'West Virginia', 'WI'=>'Wisconsin', 'WY'=>'Wyoming', 'DC'=>'Washington, D.C.'
);

foreach my $state  (keys %STATES) { 
#   warn $state;
#    next;
    $response = $ua->get(
        $app . 
        '/export/pledge/' . $state . '/csv/'
        );
    die "\nError: ", $response->status_line unless $response->is_success;
    
    print $response->as_string;
}

