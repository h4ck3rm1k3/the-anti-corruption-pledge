/*
  functions from the pledge form  
*/

function setPledge(fullname) {   
    var pledge='I do so pledge.';
    if (fullname.length >0 ) {
        pledge = 'I, ' + fullname + ', do so pledge.';
    }    

    // now update the pledge and the 
    $('#pledgebutton').text( pledge);
}

function updatePledgeNonstandard () 
{
    var fullname=$('#in_name_full').val();  
    setPledge(fullname);
}

function updatePledgeButton() {
    var nameparts= [];  
    var name_ids= [
	'#in_name_title',
	'#in_name_first',
	'#in_name_middle',
	'#in_name_last',
	'#in_name_suffix'
    ];   
    for (var i = 0; i < name_ids.length; i++) {
	var name = name_ids[i];
	var part = $(name).val();
	if ( part.length >0 )
	{
	    nameparts.push(part);
	}
    }   
    var fullname = nameparts.join (" ");    
//    console.log("new full name:" + fullname);
    $('#in_name_full').val(fullname);

    setPledge(fullname);
};

function showCustomTitle()
{
    var control = $("#in_name_title_combo");
    var nonstandard = $("#in_name_override").is(":checked");
    $("#in_name_title").val("");
    if ((control.val()) &&   (control.val() != "Other")  ) {
	$("#in_name_title").attr("readonly","true");
	$("#in_name_title").attr("disabled","true");	
	$("#tr_custom_title").hide('slow');	
    } else {
	$("#in_name_title").removeAttr('readonly');
	$("#in_name_title").removeAttr('disabled');
	if (!nonstandard) {
	    $("#tr_custom_title").show();
	}
    }    
    
    updatePledgeButton() ;
}

function updatePledgeButtonTitle()
{
    var control = $("#in_name_title_combo");
    showCustomTitle();
    if ((control.val()) &&   
	(control.val() != "Other") &&
	(control.val() != "- Select Your Title -")	
       ) {
	$("#in_name_title").val(control.val());
	$("#in_name_title").attr("readonly","true");
	$("#in_name_title").attr("disabled","true");	
    } else {
	$("#in_name_title").val("");	
	$("#in_name_title").removeAttr('readonly');
	$("#in_name_title").removeAttr('disabled');
    }
    updatePledgeButton(); // update the pledge

}

/*
from : http://www.bytemycode.com/snippets/snippet/595/
*/
function toggleClass(objClass,display){
    var ie = (document.all) ? true : false;
    var elements = (ie) ? document.all : document.getElementsByTagName('*');
    for (i=0; i<elements.length; i++){
	if (elements[i].className==objClass){
	    elements[i].style.display=display
	}
    }
}

function startup_function() 		   {
    var item=$('#in_name_override');   
    if (item.length > 0)    {
	// is this the right page?
	updatePledgeInit();
    }
}

$(document).ready( startup_function );


function updatePledgeInit() {
    //alert("start");
    showCustomTitle();
    updatePledgeButtonNonstandard();
}

function updatePledgeButtonNonstandard()
{    
    var nonstandard = $("#in_name_override").is(":checked");
    var fullname=$('#in_name_full').val();  
    var name_ids= [
	'#in_name_title',
	'#in_name_first',
	'#in_name_middle',
	'#in_name_last',
	'#in_name_suffix'
    ];
    if (nonstandard ) {
	toggleClass("cls_standard_name","none");
	showCustomTitle();
	$("#in_name_full").removeAttr('readonly');
	$("#in_name_full").removeAttr('class' );

	for (var i = 0; i < name_ids.length; i++) {
	    $(name_ids[i]).val("");
	    $(name_ids[i]).attr("readonly","true");
	    $(name_ids[i]).attr("disabled","true");	
	}	
    } else {
	toggleClass("cls_standard_name","");
	showCustomTitle();
	$("#in_name_full").attr("readonly","true");
	$("#in_name_full").addClass("lighter");
	$("#in_name_full").val("");
	for (var i = 0; i < name_ids.length; i++) {
	    $(name_ids[i]).removeAttr("readonly");
	    $(name_ids[i]).removeAttr("disabled");
	}	
    }

    updatePledgeButton() ;
}
