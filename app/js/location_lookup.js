/**
 * Lookup politicians.
 * Callback should be structured as so:
 *
 * function callback(state, district, [ bioguide_id, bioguide_id, bioguide_id, ...])
 */
function lookupPoliticians(full_address,
  geolookup_message_element,
  geolookup_error_message_element,
  politicians_lookup_message_element,
  politicians_lookup_error_message_element,
  callback) {
  if ('' != geolookup_message_element) {
    geolookup_message_element.style.display = "none";
  }
  geolookup_error_message_element.style.display = "none";
  if ('' != politicians_lookup_message_element) {
    politicians_lookup_message_element.style.display = "none";
  }
  politicians_lookup_error_message_element.style.display = "none";
  
  var state = null;
  var district = null;
  var bioguide_ids = [];
  
  var lookupGeocode = function() {
    var geocoder = new google.maps.Geocoder();
    if ('' != geolookup_message_element) {
      geolookup_message_element.style.display = "block";
    }
    geocoder.geocode(
      {
        'address': full_address
      },
      geocodeFinished);
  };
  
  var geocodeFinished = function(results, status) {
    if ('' != geolookup_message_element) {
      geolookup_message_element.style.display = "none";
    }
    if (results && results[0]) {
      if ('' != politicians_lookup_message_element) {
        politicians_lookup_message_element.style.display = "block";
      }
      var location = results[0].geometry.location;
      lookupPoliticians(location.lat(), location.lng());
    } else {
      geolookup_error_message_element.style.display = "block";
    }
  };
  
  var lookupPoliticians = function(lat, lon) {
    var lookupUri = "http://services.sunlightlabs.com/api/legislators.allForLatLong.json?apikey=5e8a32396f794486971eff338a8d759f&jsonp=lookupPoliticiansSuccess&latitude=" + lat + "&longitude=" + lon;
    var script = document.createElement("script");
    script.setAttribute("src", lookupUri);
    document.body.appendChild(script);
  };
  
  var lookupPoliticiansSuccess = function(response) {
    if ('' != politicians_lookup_message_element) {
      politicians_lookup_message_element.style.display = "none";
    }
    if (response && response.response && response.response.legislators) {
      var legislators = response.response.legislators;
      for (var i = 0; i < legislators.length; ++i) {
        var legislator = legislators[i].legislator;      
        bioguide_ids.push(legislator.bioguide_id);
        
        // All of the relevant legislators should be in the same state.
        if (legislator.state) {
          state = legislator.state;
        }
        
        // If the legislator is a Representative, then their district is relevant to the user.
        if (legislator.title == "Rep") {
          district = legislator.district;
        }
      }
      // Success!
      callback(state, district, bioguide_ids);
    } else {
      lookupPoliticiansFailure();
    }
  };
  window.lookupPoliticiansSuccess = lookupPoliticiansSuccess;  // make the callback available from the jsonp script.
  
  var lookupPoliticiansFailure = function() {
    // TODO: figure out how to determine when we simply couldn't find the relevant legislators
    politicians_lookup_error_message_element.style.display = "block";
  };
  
  // Start the process!
  lookupGeocode();
}
