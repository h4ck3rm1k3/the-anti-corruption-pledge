#!/usr/bin/env python
#http://berlinbrowndev.blogspot.de/2008/05/running-google-appengine-django-unit.html

'''
 Author: Berlin Brown
 Filename: run_all_tests.py 
 ghostnet->the-anti-corruption-pledge 
 Application: test case for the-anti-corruption-pledge 
 Environment: Python 2.5.2
 -------------------------- Description --

 With the following code, you can run google-appengine
 stand-alone database driven test cases.

 * Run batch python unit test scripts
 * The script ensures that the google libraries are imported and
   added to the python sys path.
 
'''

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import sys
import unittest

# Add the parent directory, the-anti-corruption-pledge  to the sys path.
# We are assuming that google_appengine is also in the
# For example:
# -----------------------------
# PROJECT_HOME/google_appengine
# PROJECT_HOME/projects/the-anti-corruption-pledge/tests

DIR_PATH = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
PROJECT_HOME = os.path.join(DIR_PATH, '..', '..')

print("INFO: project_home=%s" % PROJECT_HOME)

EXTRA_PATHS = [
  DIR_PATH,
  os.path.join(PROJECT_HOME, 'the-anti-corruption-pledge'),
  os.path.join(PROJECT_HOME, 'the-anti-corruption-pledge', 'app'),
  os.path.join(PROJECT_HOME, 'google_appengine'),
  os.path.join(PROJECT_HOME, 'google_appengine', 'lib', 'django'),
  os.path.join(PROJECT_HOME, 'google_appengine', 'lib', 'webob'),
  os.path.join(PROJECT_HOME, 'google_appengine', 'lib', 'yaml', 'lib')
]
sys.path = EXTRA_PATHS + sys.path
print EXTRA_PATHS

# End of path setup

import datetime
import logging

# Google App Engine includes
from google.appengine.api import apiproxy_stub_map
from google.appengine.api import datastore_file_stub
from google.appengine.api import mail_stub
from google.appengine.api import user_service_stub 
from google.appengine.ext import db

APP_ID = 'the-anti-corruption-pledge_tests'
DS_PATH = '/tmp/dev_ds_tests.db'
HIST_PATH = '/tmp/dev_ds_tests.hist'

apiproxy_stub_map.apiproxy.RegisterStub(
        'user',
        user_service_stub.UserServiceStub())

# From the dev_appserver source:
# ----------------------
#  datastore = datastore_file_stub.DatastoreFileStub(
#      app_id, datastore_path, history_path, require_indexes=require_indexes)
# ----------------------
apiproxy_stub_map.apiproxy.RegisterStub(
        'datastore_v3',
        datastore_file_stub.DatastoreFileStub('the-anti-corruption-pledge_tests',
                                              DS_PATH,
                                              HIST_PATH))
apiproxy_stub_map.apiproxy.RegisterStub(
        'mail',
        mail_stub.MailServiceStub())

# ----------------------
# Django Setups
# ----------------------

# A workaround to fix the partial initialization of Django before we are ready
from django.conf import settings
settings._target = None



# Import various parts of Django.
import django.core.handlers.wsgi
import django.core.signals
import django.dispatch.dispatcher
import django.db

# --------------
# Tests includes
# --------------
from tests.name_tests import suite as suite_names

def run_test_suite():
        suite = unittest.TestSuite()
        suite.addTest(suite_names())
        runner = unittest.TextTestRunner()
        runner.run(suite)

# End of appengine setups for command-line test apps
        
if __name__ == '__main__':
        print "Running model create"    
        run_test_suite()
        print "Done"

# End of Script
