import models
import urllib
import xml.dom.minidom

NYT_API_KEY = "29794c1f73eba2696e342022ab497677:17:65505150"
SUNLIGHT_API_KEY = "01a34fd2fc874e5681339ebce543ba06"

def getChildNodeData(doc, element):
  node = doc.getElementsByTagName(element)
  for node2 in node:
    for node3 in node2.childNodes:
      if node3.nodeType == xml.dom.minidom.Node.TEXT_NODE:
        return node3.data

def loadPoliticians():
  xml_file = urllib.urlopen("http://services.sunlightlabs.com/api/legislators.getList.xml?apikey=%s" % SUNLIGHT_API_KEY)
  doc = xml.dom.minidom.parse(xml_file)

  for node in doc.getElementsByTagName("legislator"):
    name_first = getChildNodeData(node, "firstname")
    name_last = getChildNodeData(node, "lastname")
    party = getChildNodeData(node, "party")
    state = getChildNodeData(node, "state")
    phone = getChildNodeData(node, "phone")
    website = getChildNodeData(node, "website")
    if website[-1] != "/":
      website = "%s/" % website
    webform = getChildNodeData(node, "webform")
    twitter = getChildNodeData(node, "twitter_id")
    facebook = getChildNodeData(node, "facebook_id")
    bioguide_id = getChildNodeData(node, "bioguide_id")
    chamber = getChildNodeData(node, "chamber")
    district = getChildNodeData(node, "district")

    query = models.Politician.all()
    query.filter("state =", state)
    query.filter("name_last =", name_last)
    query.filter("bioguide_id =", bioguide_id)
    results = query.fetch(limit=1)
    if len(results) == 1:
      p = results[0]
    else:
      p = models.Politician()
    p.name_first = name_first
    p.name_last = name_last
    if chamber == "senate":
      p.type = "Senator"
    else:
      p.type = "Representative"
      p.district = district
    if party == "I":
      p.party = "Independent"
    if party == "R":
      p.party = "Republican"
    if party == "D":
      p.party = "Democratic"
    p.state = state
    p.phone = phone
    p.website = website
    p.webform = webform
    p.twitter = twitter
    p.facebook = facebook
    p.bioguide_id = bioguide_id
    p.put()

def loadProposal(name, tooltip, principle):
  query = models.Proposal().all()
  query.filter("name =", name)
  query.filter("principle =", principle)
  results = query.fetch(limit=1)
  if len(results) == 1:
    p = results[0]
  else:
    p = models.Proposal() 
  p.name = name
  p.tooltip = tooltip
  p.principle = principle
  p.put()

def loadProposals():
  name = 'Fair Elections Now Act'
  tooltip = 'H.R. 1826. This bill provides matching funds for candidates who take small dollar contributions only. Learn more at <a href="http://fairelectionsnow.org/about-bill" target="_blank">FairElectionsNow</a>. <a href="http://www.govtrack.us/congress/bill.xpd?bill=h111-1826&tab=summary" target="_blank">Read the bill at GovTrack.us</a>'
  principle = 'provide'
  loadProposal(name, tooltip, principle)
  name = 'The Grant and Franklin Project'
  tooltip = 'This proposal gives a tax rebate in the form of a voucher that can be given to candidates who fund their compaign only with vouchers plus contributions of up to $100. <a href="http://www.nytimes.com/2011/11/17/opinion/in-campaign-financing-more-money-can-beat-big-money.html?_r=1">Read about the proposal at NYTimes.com</a>'
  principle = 'provide'
  loadProposal(name, tooltip, principle)
  name = 'The Occupied Amendment'
  tooltip = 'H.J. 90. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-90" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'The Occupied Amendment'
  tooltip = 'H.J. 90. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-90" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'Ellison Amendment'
  tooltip = 'H.J. 92. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-92" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'People\'s Rights Amendment'
  tooltip = 'H.J. 88. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-88" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'Sutton Amendment'
  tooltip = 'H.J. 86. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-86" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Udall Amendment'
  tooltip = 'S.J. 29. <a href="http://www.govtrack.us/congress/bill.xpd?bill=sj112-29" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Schrader Amendment'
  tooltip = 'H.J. 72. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-72" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Schrader Amendment'
  tooltip = 'H.J. 72. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-72" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'Kaptur Amendment'
  tooltip = 'H.J. 8. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj112-8" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Edwards Amendment'
  tooltip = 'H.J. 74. <a href="http://www.govtrack.us/congress/bill.xpd?bill=hj111-74" target="_blank">Read the resolution at GovTrack.us</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Lyons Amendment (Vermont)'
  tooltip = 'J.R.S. 11. <a href="http://www.leg.state.vt.us/database/status/summary.cfm?Bill=JRS011&Session=2012" target="_blank">Read the resolution</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'Move to Amend Amendment'
  tooltip = 'Move to Amend 28th Amendment. <a href="http://movetoamend.org/amendment" target="_blank">Learn more</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'Move to Amend Amendment'
  tooltip = 'Move to Amend 28th Amendment. <a href="http://movetoamend.org/amendment" target="_blank">Learn more</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'Get Money Out Amendment'
  tooltip = 'Get Money Out. <a href="http://www.getmoneyout.com/" target="_blank">Learn more</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)
  name = 'WolfPAC Amendment'
  tooltip = '28th Amendment, WolfPAC. <a href=" http://www.wolf-pac.com/28th" target="_blank">Learn more</a>'
  principle = 'limit'
  loadProposal(name, tooltip, principle)
  name = 'WolfPAC Amendment'
  tooltip = '28th Amendment, WolfPAC. <a href=" http://www.wolf-pac.com/28th" target="_blank">Learn more</a>'
  principle = 'reaffirm'
  loadProposal(name, tooltip, principle)

def loadGooglePlus(name_last, state, googleplus):
  query = models.Politician.all()
  query.filter("state = ", state)
  query.filter("name_last = ", name_last)
  results = query.fetch(limit=1)
  if len(results) == 1:
    p = results[0]
    p.googleplus = googleplus
    p.put()

def loadGooglePlusAccounts():
  loadGooglePlus("Sanders", "VT", "1022278000261183349957")
  loadGooglePlus("Pelosi", "CA", "108029440032077065451")
  loadGooglePlus("Becerra", "CA", "101792748296125360291")
  loadGooglePlus("Berkley", "NV", "103723137533161601713")
  loadGooglePlus("Himes", "CT", "100884334304038310069")
  loadGooglePlus("McDermott", "WA", "110504484924017189134")
  loadGooglePlus("McGovern", "MA", "116959949419892235884")
  loadGooglePlus("Paul", "TX", "118337959785760721399")
  loadGooglePlus("Peters", "MI", "112171590729436134540")
  loadGooglePlus("Schakowsky", "IL", "114360330721351983428")
  loadGooglePlus("Walsh", "IL", "102422689553378328266")
  loadGooglePlus("Hatch", "UT", "109807016750012045932")
  loadGooglePlus("Heller", "NV", "107598107118076751267")
  loadGooglePlus("Udall", "CO", "108648232578397000685")
  loadGooglePlus("Warner", "VA", "113688051093983572899")
  loadGooglePlus("Coons", "DE", "101703082060192373999")
  loadGooglePlus("Boehner", "OH", "107913505678621875336")
  loadGooglePlus("Giffords", "AZ", "106297613785833705111")
