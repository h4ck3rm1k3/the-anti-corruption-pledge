import os
import load
import logging
import models
import os
import uuid
import webapp2
from nameparser import HumanName

from google.appengine.api import files
from google.appengine.api import mail
from google.appengine.api import users
from google.appengine.api import users

from google.appengine.ext import blobstore
from google.appengine.ext import db

from google.appengine.ext.webapp import template

import logging
log = logging.getLogger('CSV Export')

class ExportPledgeCSV(webapp2.RequestHandler):

    def export_state(self, state): 
        # Create the file
        file_name = files.blobstore.create(     _blobinfo_uploaded_filename="Export of Data for state %s" % state,      mime_type='application/octet-stream')
        f = files.open(file_name, 'a')

        if f is None :
            log.debug( "<h1>ERROR cannot open %s</h1>" % file_name)
            self.response.out.write("<h1>ERROR cannot open %s</h1>" % file_name) 
            return
        
        log.debug("File:")
        log.debug( f)
        
        # if not users.is_current_user_admin():
        #     self.response.set_status(403,message="Forbidden")
        #     self.response.headers['Content-type'] = 'text/html'        
        #     self.response.out.write('<h1>Forbidden</h1>') # emit it
        #     return

        log.debug( '<h1>Starting to write</h1>')
        self.response.out.write('<h1>Starting to write</h1>') # emit it
        """ Returns a CSV output per state."""

#            pledges = models.Pledger.all()
#        else:
        pledges = models.Pledger.all().filter("state = ", state)

        self.response.headers['Content-type'] = 'text/plain'        

        # this is a list of the fields, they are emitted in that order
        fieldnames = [
            # string fields 
            'state',
            'district', 
            'email',
            'name_full',
            #date
            'pledge_date',
            # list
            'supported_proposals',
            #bool
            'is_politician',
            'has_pledged',
            'has_pledged_limit',
            'has_pledged_provide',
            'has_pledged_reaffirm',
            'has_pledged_revolving',
            # politician fields... waste, should be in a different table
            'politician_district',
            'politician_party',
            'politician_state',
            'politician_type',
            ]

        # append the field names for the automatically derived fields
        titles = list(fieldnames) # start with the original list 
        for x in models.Pledger.human_fieldnames: #for each human name field
            titles.append( "HumanName_%s" % x) # build up unique titles
                  
        #now we append the field names for the native name fields.
        for fld in models.Pledger.human_fieldnames:
            titles.append( "NativeName_%s" % fld) # build up unique titles

        # emit the header row of the csv
        f.write('"%s"\n' % '", "'.join(titles)) # field name

        ############# body of the data
        # loop over all the data rows
        count=0
        for p in pledges: # for the filtered pledged
            values = []
            for fld in fieldnames:
                value = getattr( p, fld) # get the field with that name
                valstr="%s" % value # convert to string
                values.append(valstr) # append to a list

            name = None
            # split the name in a human way 
            if (p.name_full) :
                name = HumanName(p.name_full)
                 
            for fld in models.Pledger.human_fieldnames:
                if (name):
                    value = getattr( name, fld) # get the human name field with that field name
                    valstr="%s" % (value) # convert to string
                else:
                    valstr="" 
                values.append(valstr) # append to a list


            #add in the native name parts
            for fld in models.Pledger.human_fieldnames:
                fieldname = "name_%s" % fld
                value = getattr( p, fieldname ) # get the human name field with that field name
                if (value) :
                    if (value.__len__()) :
                        valstr="%s" % value # convert to string
                        values.append(valstr)
                    else:
                        valstr=""
                        values.append(valstr)

            csvstr= '", "'.join(values) # join into a csv sting

            if (count % 10000 ==0):
                log.debug( 'processed 10000')
                self.response.out.write('<p>.</p>') # emit it            
            f.write(('"%s"\n') % csvstr) # emit it
            count = count + 1

        log.debug( 'Filename:%s' % file_name)
        self.response.out.write('<p>%s</p>' % file_name) # emit it            
        f.close()
        files.finalize(file_name)

        log.debug( 'Filename:%s' % file_name)
        blob_key = files.blobstore.get_blob_key(file_name)

        if blob_key is not None :           
            log.debug( 'Blob Key:%s' % blob_key)
            self.response.out.write('<p>%s</p>' % blob_key) # emit it            
            blob_info = blobstore.BlobInfo.get(blob_key)


    def get(self, state): 

        if (state == 'ALL') :
            for s in models.STATES.keys():
                log.debug( 'processing state :%s' % s)
                self.export_state( s)
        else:
            self.export_state( state)


class ExportPledgeCSVList(webapp2.RequestHandler):
#https://developers.google.com/appengine/docs/python/blobstore/blobinfoclass#BlobInfo_all
#https://developers.google.com/appengine/docs/python/blobstore/blobinfoclass#BlobInfo_filename
#https://developers.google.com/appengine/docs/python/blobstore/overview
#https://developers.google.com/appengine/docs/python/blobstore/overview#Serving_a_Blob
#http://stackoverflow.com/questions/9367037/generate-zip-files-and-store-in-gae-blobstore

    def get(self ):
        blobs = blobstore.BlobInfo.all().fetch(500)
        self.response.out.write('<h1>All States</h1>')
        for blob in blobs:
            name =blob.filename
#            self.response.out.write('<p>%s</p>' % name)
            if  'Export of Data for state' in name :
                href='<a href=/export/dl/%s>%s</a>' % (blob.key(), name)
                self.response.out.write('<p>%s : %s</p>' % (name,href))
                
                #blob.key(),
#                log.debug(blob.key());


from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
#webapp2.RequestHandler
class ExportPledgeCSVDL(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, resource):
        blob_info = blobstore.BlobInfo.get(resource)
        self.send_blob(blob_info)
