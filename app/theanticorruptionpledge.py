import os
import load
import logging
import models
import os
import uuid
from google.appengine.api import mail
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp import template
import webapp2

# export csv 
from  theanticorruptionpledge_csv_export import ExportPledgeCSV
from  theanticorruptionpledge_csv_export import ExportPledgeCSVList
from  theanticorruptionpledge_csv_export import ExportPledgeCSVDL

# bugfix so it can be used on a localhost, no hardcoded hostname
from urlparse import urlparse




template.register_template_library('templatefilters.templatefilters')

class BaseRequestHandler(webapp2.RequestHandler):
    """ Base handler for common templating needs. """
    def render(self, template_name, template_values={}):
        template_values.update({
            'request': self.request,
            'user': users.get_current_user(),
            'login_url': users.create_login_url(self.request.uri),
            'logout_url': users.create_logout_url(self.request.uri),
            'is_admin': users.is_current_user_admin()
        })
        directory = os.path.dirname(__file__)
        path = os.path.join(directory, 'templates', template_name)
        self.response.out.write(template.render(path, template_values))

    def require_login(self):
        if not users.get_current_user():
            self.redirect(users.create_login_url(self.request.uri))
            return False
        return True

    def require_admin(self):
        if not users.is_current_user_admin():
            self.redirect(users.create_login_url(self.request.uri))
            return False
        return True


class MainPage(BaseRequestHandler):
    def get(self):
        state_counts = {}
        for state in models.STATES.keys():
            state_counts[models.STATES[state]] = models.get_count('pledge_%s_count' % state) 
        provide_proposals = models.Proposal.all().filter("principle = ", "provide").order("name")
        limit_proposals = models.Proposal.all().filter("principle = ", "limit").order("name")
        reaffirm_proposals = models.Proposal.all().filter("principle = ", "reaffirm").order("name")
        template_values = {
            'nav_main_page': True,
            'pledge_count': models.get_count('pledge_count'),
            'states': models.STATES.values(),
            'state_counts': state_counts,
            'max_districts': range(1, 56),
            'provide_proposals': provide_proposals,
            'limit_proposals': limit_proposals,
            'reaffirm_proposals': reaffirm_proposals
        }
        self.render('index.html', template_values)

class TakeThePledge(BaseRequestHandler):
    def get(self):
        # Display pledge form
        state_counts = {}
        for state in models.STATES.keys():
            state_counts[models.STATES[state]] = models.get_count('pledge_%s_count' % state) 
        provide_proposals = models.Proposal.all().filter("principle = ", "provide").order("name")
        limit_proposals = models.Proposal.all().filter("principle = ", "limit").order("name")
        reaffirm_proposals = models.Proposal.all().filter("principle = ", "reaffirm").order("name")
        template_values = {
            'nav_take_the_pledge': True,
            'pledge_count': models.get_count('pledge_count'),
            'states': models.STATES.values(),
            'state_counts': state_counts,
            'max_districts': range(1, 56),
            'provide_proposals': provide_proposals,
            'limit_proposals': limit_proposals,
            'reaffirm_proposals': reaffirm_proposals
        }
        self.render('take_the_pledge.html', template_values)


    def current_site_url(self):
        # http://stackoverflow.com/questions/2764586/get-current-url-in-python
        # http://browse-tutorials.com/tutorial/get-selfbase-url-python
        return urlparse(self.request.url).netloc
    


    def post(self):

        # Process pledge form
        how_many_of_same_email = models.Pledger.all().filter("email = ", self.request.get('email')).count()
        if(how_many_of_same_email > 0):
            # Duplicate; log error and display error message
            logging.error("WE HAVE A DUPLICATE!!!!")
            error_message = "The person with the email address %s has already pledged." % self.request.get('email')
            provide_proposals = models.Proposal.all().filter("principle = ", "provide").order("name")
            limit_proposals = models.Proposal.all().filter("principle = ", "limit").order("name")
            reaffirm_proposals = models.Proposal.all().filter("principle = ", "reaffirm").order("name")
            template_values = {
                "max_districts": range(1, 56),
                "provide_proposals": provide_proposals,
                "limit_proposals": limit_proposals,
                "reaffirm_proposals": reaffirm_proposals,
                "error_message": error_message,
            }
            self.render('take_the_pledge.html', template_values)
        else:    
            # Not a duplicate; proceed
            p = models.Pledger()
    
#            logging.error("name full:" + pp.pformat(self.request.get('name_full')) )

            if self.request.get('name_full'):
                p.name_full = self.request.get('name_full')

            if self.request.get('name_title'):
                p.name_title = self.request.get('name_title')

            if self.request.get('name_first'):
                p.name_first = self.request.get('name_first')

            if self.request.get('name_middle'):
                p.name_middle = self.request.get('name_middle')

            if self.request.get('name_last'):
                p.name_last = self.request.get('name_last')

            if self.request.get('name_suffix'):
                p.name_suffix = self.request.get('name_suffix')


            p.state = self.request.get('state')
            p.district = self.request.get('district')
            if self.request.get('email'):
                p.email = self.request.get('email')
            if self.request.get('pledged_provide'):
                p.has_pledged_provide = True
            if self.request.get('pledged_revolving'):
                p.has_pledged_revolving = True
            if self.request.get('supported_proposal_provide'):
                proposals = self.request.get_all('supported_proposal_provide')
                for proposal in proposals:
                    p.supported_proposals.append(db.get(proposal).key())
            if self.request.get('pledged_limit'):
                p.has_pledged_limit = True
            if self.request.get('supported_proposal_limit'):
                proposals = self.request.get_all('supported_proposal_limit')
                for proposal in proposals:
                    p.supported_proposals.append(db.get(proposal).key())
            if self.request.get('pledged_reaffirm'):
                p.has_pledged_reaffirm = True
            if self.request.get('supported_proposal_reaffirm'):
                proposals = self.request.get_all('supported_proposal_reaffirm')
                for proposal in proposals:
                    p.supported_proposals.append(db.get(proposal).key())
            if self.request.get('is_politician'):
                p.is_politician = True
                p.politician_type = self.request.get('politician_type')
                p.politician_party = self.request.get('politician_party')
                p.politician_state = self.request.get('politician_state')
                if ((p.politician_type == 'Candidate') and (self.request.get('politician_chamber') == 'House')) or (p.politician_type == 'Representative'):
                    p.politician_district = self.request.get('politician_district')
            p.verification_code = uuid.uuid1().hex
            p.put()


    
            the_body = """Dear %s:

In order to complete your pledge, you must verify your email address. Please click
the following link, or paste it into your browser:

%s

The Anti Corruption Pledge Team
                """ % (p.compose_name(), self.current_site_url() + "/verify/" + p.verification_code)
            mail.send_mail(sender="support@theanticorruptionpledge.org",
                                        to=p.email,
                                        subject="Please verify your email address",
                                        body=the_body)
    
            logging.error("body: " + the_body)
            self.redirect('/embedthepledge?k=%s' % p.key())


class HelpTheCause(BaseRequestHandler): 
    def get(self):
        template_values = {'nav_help_the_cause': True}
        self.render('help_the_cause.html', template_values)


class EmbedPage(BaseRequestHandler):
    """ Render the embeddable pledge badge. """
    def get(self):
        if not self.request.get('k'):
            self.response.headers['Content-type'] = 'text/html'
            self.response.out.write('<html><head></head><body>Not available.</body></html>')
            return
        key = self.request.get('k')
        orientation = self.request.get('o')
        pledger = db.get(key)
        pledges = []
        if pledger.has_pledged_provide:
            pledges.append('provide')
        if pledger.has_pledged_limit:
            pledges.append('limit')
        if pledger.has_pledged_reaffirm:
            pledges.append('reaffirm')
        if pledger.has_pledged_revolving:
            pledges.append('revolving')
        provide_proposals = []
        limit_proposals = []
        reaffirm_proposals = []
        revolving_proposals = []
        for proposal_key in pledger.supported_proposals:
            proposal = db.get(proposal_key)
            if proposal.principle == 'provide':
                provide_proposals.append(proposal)
            elif proposal.principle == 'limit':
                limit_proposals.append(proposal)
            elif proposal.principle == 'reaffirm':
                reaffirm_proposals.append(proposal)
            elif proposal.principle == 'revolving':
                revolving_proposals.append(proposal)
        pledge_badge_dimension = 142 + (len(pledges)-1) * 29
        template_values = {
            'has_pledged': pledger.has_pledged,
            'pledges': pledges,
            'orientation': orientation,
            'provide_proposals': provide_proposals,
            'limit_proposals': limit_proposals,
            'reaffirm_proposals': reaffirm_proposals,
            'revolving_proposals': revolving_proposals,
            'pledge_badge_dimension': pledge_badge_dimension
        }
        self.render('embed.html', template_values)


class EmbedThePledgePage(BaseRequestHandler):
    """ Give the user his/her code to embed the pledge """
    def get(self):
        if not self.request.get('k'):
            self.redirect('/')
            return
        key = self.request.get('k')
        pledger = db.get(key)
        pledges = []
        if pledger.has_pledged_provide:
            pledges.append('provide')
        if pledger.has_pledged_limit:
            pledges.append('limit')
        if pledger.has_pledged_reaffirm:
            pledges.append('reaffirm')
        if pledger.has_pledged_revolving:
            pledges.append('revolving')
        supported_proposals = []
        for proposal_key in pledger.supported_proposals:
            supported_proposals.append(db.get(proposal_key))
        pledge_badge_dimension = 142 + (len(pledges)-1) * 29
        template_values = {
            'nav_take_the_pledge': True,
            'k': key,
            'pledger': pledger,
            'pledges': pledges,
            'supported_proposals': supported_proposals,
            'pledge_badge_dimension': pledge_badge_dimension
        }
        self.render('embedthepledge.html', template_values)


class VerifyEmailPage(BaseRequestHandler):
  def get(self,verification_code):
    # lookup verification code
    pledger = models.Pledger.all().filter("verification_code = ", verification_code).get()

    if (not(pledger)) :
        logging.error("Unknon Code!!!!")
        html = "<html><body>Invalid Verification Code.</body></html>" 
        return HttpResponse(html)

    pledger.has_verified = True
    pledger.verification_code = None
    pledger.put()

    models.increment('pledge_count')
    models.increment('pledge_%s_count' % pledger.state)
    
    pledges = []
    if pledger.has_pledged_provide:
      pledges.append('provide')
    if pledger.has_pledged_limit:
      pledges.append('limit')
    if pledger.has_pledged_reaffirm:
      pledges.append('reaffirm')
    if pledger.has_pledged_revolving:
      pledges.append('revolving')
    supported_proposals = []
    for proposal_key in pledger.supported_proposals:
        supported_proposals.append(db.get(proposal_key))
    pledge_badge_dimension = 142 + (len(pledges)-1) * 29
    orientation = ''
    if (len(pledges) > 0):
      orientation = '&o=h'
      
    """ Send follow-up email """

    message = mail.EmailMessage(sender="The Anti-Corruption Pledge <support@theanticorruptionpledge.org>",
                                subject="Thanks for taking the pledge")

    message.to = pledger.email

    message.body = """
Hi,

Thank you for taking the anti-corruption pledge.

This is a completely grass-roots movement, built by volunteers and activists, and we need your help to make it grow. We expect this will grow in two stages: First, by multiplying the number of pledges by citizens, and then second, by using that number within particular districts to get the Representatives and candidates for Congress and the Senate to take the pledge themselves.

Please help us with both if you can. You can share a link to the site on Twitter (http://bit.ly/wODxou) and Facebook (http://on.fb.me/zHjzgC). And you can ask your Representative to take the pledge here: http://www.theanticorruptionpledge.org/help-the-cause/.

Finally, if you want to do more still, we could use whatever you can donate to promote the site and spread the word: http://rootstrikers.org/Donate/

100%% of the money we raise will go to spreading the word. None goes to overhead.

Thank you again for your support. There is a lot that must be done to end this corruption. You've helped us take the first step.

Embed the Pledge (copy and paste into your website):

<textarea id="embedv" rows="4" cols="50"><iframe allowTransparency="true" frameborder="0" scrolling="no" src="http://%s/embed?k=%s%s" style="border: none; overflow: hidden; height: 52px; width: %s;"></iframe></textarea>

Lawrence Lessig
#Rootstriker
    """ % (self.request.host, pledger.key(), orientation, pledge_badge_dimension)

# todo: update fb and twitter links

    message.html = """
<html><head></head><body>

<p>Hi,</p>

<p>Thank you for taking the anti-corruption pledge.</p>

<p>This is a completely grass-roots movement, built by volunteers and activists, and we need your help to make it spread. We expect this will grow in two stages: First, by multiplying the number of pledges by citizens, and then second, by using that number within particular districts to get the Representatives and candidates for Congress and the Senate to take the pledge themselves.</p>

<p>Please help us with both if you can. You can share a link to the site on <a href="http://bit.ly/wODxou">Twitter</a> and <a href="http://on.fb.me/zHjzgC">Facebook</a>. And you can ask your Representative to take the pledge <a href="http://www.theanticorruptionpledge.org/help-the-cause/">here</a>.</p>

<p>Finally, if you want to do more still, we could use whatever you can <a href="http://rootstrikers.org/Donate/">donate</a> to promote the site and spread the word. 100%% of the money we raise will go to spreading the word. None goes to overhead.</p>

<p>Thank you again for your support. There is a lot that must be done to end this corruption. You've helped us take the first step.</p>

Embed the Pledge (copy and paste into your website):

<textarea id="embedv" rows="4" cols="50"><iframe allowTransparency="true" frameborder="0" scrolling="no" src="http://%s/embed?k=%s%s" style="border: none; overflow: hidden; height: 52px; width: %s;"></iframe></textarea>

<p>Lawrence Lessig<br />
#Rootstriker</p>
    </body></html>
    """ % (self.request.host, pledger.key(), orientation, pledge_badge_dimension)

    message.send()

    template_values = {
      'nav_take_the_pledge': True,
      'k': pledger.key(),
      'pledger': pledger,
      'pledges': pledges,
      'supported_proposals': supported_proposals,
      'pledge_badge_dimension': pledge_badge_dimension
    }
    self.render('email_verification.html', template_values)

class StatePage(BaseRequestHandler):
    def get(self, state):
        if state not in models.STATES.keys():
            self.redirect("/")
        politicians = models.Politician.all().filter("state = ", state).order("name_last")
        template_values = {
            'state': state,
            'state_name': models.STATES[state],
            'politicians': politicians,
        }
        self.render('state.html', template_values)


class Load(BaseRequestHandler):
    def get(self):
        if self.require_admin():
            load.loadProposals()
            load.loadPoliticians()
            load.loadGooglePlusAccounts()
            self.redirect("/")

class PoliticianPage(BaseRequestHandler):
    def get(self, bioguide_id):
        """ Returns a HTML snippet that can be appended to any block HTML element to display politician info."""
        politician = models.Politician.all().filter("bioguide_id = ", bioguide_id)
        template_values = {
            "politician": politician.get(),
        }
        self.render('politician.html', template_values)

class EmbedStaticPledge(BaseRequestHandler):
    def get(self):
        """ Returns the static embed pledge. """
        self.render('embed_static.html')
        
app = webapp2.WSGIApplication(
                                     [('/', MainPage),
                                      ('/take-the-pledge/', TakeThePledge),
                                      ('/help-the-cause/', HelpTheCause),
                                      ('/embed', EmbedPage),
                                      ('/embedthepledge', EmbedThePledgePage),
                                      ('/embedstaticpledge', EmbedStaticPledge),
                                      ('/verify/(.*)',VerifyEmailPage),
                                      ('/load', Load),
                                      (r'/state/(.*)', StatePage),                                      
                                      (r'/politician/(.*)', PoliticianPage),
                                      ('/export/pledge/(.*)/csv/', ExportPledgeCSV), # export to csv, only for admin
                                      ('/export/list', ExportPledgeCSVList), 
                                      ('/export/dl/(.*)',  ExportPledgeCSVDL),
                                     ], debug=True)




#def main():
#  run_wsgi_app(application)
#if __name__ == "__main__":
#  main()
