use WWW::Google::ClientLogin;
use HTTP::Cookies;
use LWP::UserAgent;
#use LWP::Protocol::https; # preload
use HTTP::Request::Common qw(POST);

#see http://johannilsson.com/2011/04/13/authenticated-requests-on-app-engine.html
# 
my $user = shift @ARGV;
my $pwd = shift @ARGV;
 
my $client = WWW::Google::ClientLogin->new(
    email    => $user,
    password => $pwd,
    service  => 'ac2dm',
);
 
my $res = $client->authenticate;
die $res->status_line if $res->is_error;
 
my $auth_token = $res->auth_token;
my $app="http://3.theanticorruptionpledge.appspot.com/";

# see https://groups.google.com/forum/?fromgroups=#!msg/iplay4e/bWOKc9nO6ZY/7K-tkNtq570J
my $appauth = $app . "_ah/login";
my $ua = LWP::UserAgent->new(
    
    verify_hostname => 0,
#        SSL_cert_file   => $ssl_cert_file,
#        SSL_key_file    => $ssl_key_file,
    
    );

$ua->cookie_jar(HTTP::Cookies->new(file => "cookies.txt", autosave =>
1));

warn "app auth:$appauth";
warn "auth token:$auth_token";
my $getauth =$appauth . '?continue=' . $app . '&auth=' . $auth_token  ;
warn $getauth;

#https://accounts.google.com/ServiceLogin?service=ah&passive=true&continue=https://appengine.google.com/_ah/conflogin%3Fcontinue%3Dhttps://3.theanticorruptionpledge.appspot.com/export/pledge/RI/csv/&ltmpl=gm&shdf=CiYLEgZhaG5hbWUaGlRoZSBBbnRpLUNvcnJ1cHRpb24gUGxlZGdlDBICYWgiFJEeIklWvgF62yGy2aZBThhL6a96KAEyFMOkNNpxjswErOrLAA31Hva0vxeW

my $response = $ua->get( $getauth  );

die "\nError: ", $response->status_line unless $response->is_success;

my %STATES = {
    'AL'=>'Alabama', 
    'AK'=>'Alaska', 
    'AS'=>'American Samoa', 
};

#
my %STATESw = {
    'AL'=>'Alabama', 
    'AK'=>'Alaska', 
    'AS'=>'American Samoa', 
    'AZ'=>'Arizona', 'AR'=>'Arkansas', 'CA'=>'California', 'CO'=>'Colorado', 'CT'=>'Connecticut', 'DC'=>'District of Columbia', 'DE'=>'Delaware', 'FL'=>'Florida', 'GA'=>'Georgia', 'GU'=>'Guam', 'HI'=>'Hawaii', 'ID'=>'Idaho', 'IL'=>'Illinois', 
    'IN'=>'Indiana', 
    'IA'=>'Iowa', 'KS'=>'Kansas', 'KY'=>'Kentucky', 'LA'=>'Louisiana', 'ME'=>'Maine', 
    'MD'=>'Maryland', 'MA'=>'Massachusetts', 'MI'=>'Michigan', 'MN'=>'Minnesota', 'MS'=>'Mississippi', 
    'MO'=>'Missouri', 'MP'=>'Northern Mariana Islands', 'MT'=>'Montana', 'NE'=>'Nebraska', 'NV'=>'Nevada', 
    'NH'=>'New Hampshire', 'NJ'=>'New Jersey', 'NM'=>'New Mexico', 'NY'=>'New York', 'NC'=>'North Carolina', 
    'ND'=>'North Dakota', 'OH'=>'Ohio', 'OK'=>'Oklahoma', 'OR'=>'Oregon', 'PA'=>'Pennsylvania', 'PR'=>'Puerto Rico', 'RI'=>'Rhode Island', 'SC'=>'South Carolina', 'SD'=>'South Dakota', 'TN'=>'Tennessee', 'TX'=>'Texas', 'UT'=>'Utah', 'VT'=>'Vermont', 'VA'=>'Virginia', 'VI'=>'Virgin Islands',
    'WA'=>'Washington', 'WV'=>'West Virginia', 'WI'=>'Wisconsin', 'WY'=>'Wyoming', 'DC'=>'Washington, D.C.'
};

foreach my $state  (keys %STATES) { 
    $response = $ua->get(
        $app . 
        '/export/pledge/' . $state . '/csv/'
        );
    die "\nError: ", $response->status_line unless $response->is_success;
    
    print $response->as_string;
}

