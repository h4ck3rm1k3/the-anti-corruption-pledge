VERSION = (0, 1, 1)
__version__ = '.'.join(map(str, VERSION))
__author__ = "James Michael DuPont"
__author_email__ = 'jamesmikedupont@gmail.com'
__url__ = "bitbucket.org/h4ck3rm1k3/the-anti-corruption-pledge/"
