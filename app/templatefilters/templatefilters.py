import re
from google.appengine.ext import webapp

register = webapp.template.create_template_register()

def isnull(value):
  return (value == None)

register.filter(isnull)

def lengthis(value, arg):
  return (len(value) == int(arg))

register.filter(lengthis)

@register.simple_tag
def active(request, pattern):
  if re.search(pattern, request.path):
    return 'active'
  return ''
