from google.appengine.api import memcache
from google.appengine.ext import db
import random

STATES = {'AL': 'Alabama', 'AK': 'Alaska', 'AS': 'American Samoa', 'AZ': 'Arizona', 'AR': 'Arkansas', 'CA': 'California', 'CO': 'Colorado', 'CT': 'Connecticut', 'DC': 'District of Columbia', 'DE': 'Delaware', 'FL': 'Florida', 'GA': 'Georgia', 'GU': 'Guam', 'HI': 'Hawaii', 'ID': 'Idaho', 'IL': 'Illinois', 'IN': 'Indiana', 'IA': 'Iowa', 'KS': 'Kansas', 'KY': 'Kentucky', 'LA': 'Louisiana', 'ME': 'Maine', 'MD': 'Maryland', 'MA': 'Massachusetts', 'MI': 'Michigan', 'MN': 'Minnesota', 'MS': 'Mississippi', 'MO': 'Missouri', 'MP': 'Northern Mariana Islands', 'MT': 'Montana', 'NE': 'Nebraska', 'NV': 'Nevada', 'NH': 'New Hampshire', 'NJ': 'New Jersey', 'NM': 'New Mexico', 'NY': 'New York', 'NC': 'North Carolina', 'ND': 'North Dakota', 'OH': 'Ohio', 'OK': 'Oklahoma', 'OR': 'Oregon', 'PA': 'Pennsylvania', 'PR': 'Puerto Rico', 'RI': 'Rhode Island', 'SC': 'South Carolina', 'SD': 'South Dakota', 'TN': 'Tennessee', 'TX': 'Texas', 'UT': 'Utah', 'VT': 'Vermont', 'VA': 'Virginia', 'VI': 'Virgin Islands', 'WA': 'Washington', 'WV': 'West Virginia', 'WI': 'Wisconsin', 'WY': 'Wyoming', 'DC': 'Washington, D.C.'}
POLITICIAN_TYPES = set(["Candidate", "Representative", "Senator"])
PARTIES = set(["Democratic", "Republican", "Independent", "Green", "Moderate", "Libertarian", "Unaffiliated"])


class Politician(db.Model):
  name_first = db.StringProperty()
  name_last = db.StringProperty()
  type = db.StringProperty(choices=POLITICIAN_TYPES)
  party = db.StringProperty(choices=PARTIES)
  state = db.StringProperty(choices=set(STATES.keys()))
  district = db.StringProperty()
  phone = db.PhoneNumberProperty()
  email = db.EmailProperty()
  website = db.LinkProperty()
  webform = db.LinkProperty()
  twitter = db.StringProperty()
  facebook = db.StringProperty()
  googleplus = db.StringProperty()
  bioguide_id = db.StringProperty()
  has_pledged = db.BooleanProperty(default=False)
  has_pledged_provide = db.BooleanProperty(default=False)
  has_pledged_limit = db.BooleanProperty(default=False)
  has_pledged_reaffirm = db.BooleanProperty(default=False)
  has_pledged_revolving = db.BooleanProperty(default=False)
  pledge_date = db.DateTimeProperty()


class Pledger(db.Model):

  # the full name can be overridden by the user or is automatically generated.
  name_full = db.StringProperty()

  # see also https://code.djangoproject.com/ticket/5638
  name_title= db.StringProperty()
  name_first= db.StringProperty()
  name_middle= db.StringProperty()
  name_last= db.StringProperty()
  name_suffix= db.StringProperty()

  human_fieldnames = [
    'title',
    'first',
    'middle',
    'last',
    'suffix'
    ]

  def compose_name (self) :
    result = []
    for f in self.human_fieldnames:
      fieldname = "name_%s" % f
      value = getattr( self, fieldname ) # get the human name field with that field name
      if (value) :
        if (value.__len__()) :
          valstr="%s" % value # convert to string
          result.append(valstr)
    return " ".join(result)

  email = db.StringProperty()
  state = db.StringProperty(choices=set(STATES.keys()))
  district = db.StringProperty()
  has_pledged = db.BooleanProperty(default=True)
  has_pledged_provide = db.BooleanProperty(default=False)
  has_pledged_limit = db.BooleanProperty(default=False)
  has_pledged_reaffirm = db.BooleanProperty(default=False)
  has_pledged_revolving = db.BooleanProperty(default=False)
  pledge_date = db.DateTimeProperty(auto_now_add=True)
  is_politician = db.BooleanProperty(default=False)
  politician_type = db.StringProperty(choices=POLITICIAN_TYPES)
  politician_party = db.StringProperty(choices=PARTIES)
  politician_state = db.StringProperty(choices=set(STATES.keys()))
  politician_district = db.StringProperty()
  supported_proposals = db.ListProperty(db.Key)
  has_verified = db.BooleanProperty(default=False)
  verification_code = db.StringProperty()


class Proposal(db.Model):
  name = db.StringProperty()
  tooltip = db.StringProperty()
  principle = db.StringProperty()


# Counter code from http://code.google.com/appengine/articles/sharding_counters.html

class GeneralCounterShardConfig(db.Model):
    """Tracks the number of shards for each named counter."""
    name = db.StringProperty(required=True)
    num_shards = db.IntegerProperty(required=True, default=20)


class GeneralCounterShard(db.Model):
    """Shards for each named counter"""
    name = db.StringProperty(required=True)
    count = db.IntegerProperty(required=True, default=0)


def get_count(name):
    """Retrieve the value for a given sharded counter.

    Parameters:
      name - The name of the counter
    """
    total = memcache.get(name)
    if total is None:
        total = 0
        for counter in GeneralCounterShard.all().filter('name = ', name):
            total += counter.count
        memcache.add(name, total, 60)
    return total


def increment(name):
    """Increment the value for a given sharded counter.

    Parameters:
      name - The name of the counter
    """
    config = GeneralCounterShardConfig.get_or_insert(name, name=name)
    def txn():
        index = random.randint(0, config.num_shards - 1)
        shard_name = name + str(index)
        counter = GeneralCounterShard.get_by_key_name(shard_name)
        if counter is None:
            counter = GeneralCounterShard(key_name=shard_name, name=name)
        counter.count += 1
        counter.put()
    db.run_in_transaction(txn)
    # does nothing if the key does not exist
    memcache.incr(name)


def increase_shards(name, num):
    """Increase the number of shards for a given sharded counter.
    Will never decrease the number of shards.

    Parameters:
      name - The name of the counter
      num - How many shards to use

    """
    config = GeneralCounterShardConfig.get_or_insert(name, name=name)
    def txn():
        if config.num_shards < num:
            config.num_shards = num
            config.put()
    db.run_in_transaction(txn)
