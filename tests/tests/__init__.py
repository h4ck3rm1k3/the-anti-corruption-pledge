VERSION = (0, 0, 1)
__version__ = '.'.join(map(str, VERSION))
__author__ = "James Michael DuPont"
__author_email__ = 'JamesMikeDuPont@gmail.com'
__license__ = "GPL"
__url__ = "https://bitbucket.org/rootstrikers/the-anti-corruption-pledge/"

import name_tests
